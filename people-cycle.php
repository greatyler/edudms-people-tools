<?php

function edudms_pt_people_cycle($member_type = 'faculty', $sort_by = 'first_name', $field = 'default') {


	$args = array(
		'blog_id'      => $GLOBALS['blog_id'],
		'role'         => '',
		'meta_key'     => 'edudms_pt_member_type',
		'meta_value'   => $member_type,
		'meta_compare' => '',
		'meta_query'   => array(),
		'date_query'   => array(),        
		'include'      => array(),
		'exclude'      => array(),
		'offset'       => '',
		'search'       => '',
		'number'       => '',
		'count_total'  => false,
		'fields'       => 'all',
		'who'          => '',
	); 

$edudms_pt_cycle = get_users( $args );
	
	
	usort($edudms_pt_cycle, create_function('$a, $b', 'return strnatcasecmp($a->' . $sort_by . ', $b->' . $sort_by . ');'));	
	
	return $edudms_pt_cycle;

	
}


function edudms_pt_header_output($headers) {
	edudms_pt_headers_start();
		$edudms_pt_name_stack = get_option('edudms_pt_name_stack');
		$headers_array = comma_delimited_parser($headers);
		foreach($headers_array as $header) {
			if($header !== 'Title' and $edudms_pt_name_stack == '1') {
				echo '<div class="' . $header . ' edudms_pt_pp_header';
					if($header == 'Name' and $edudms_pt_name_stack == '1') {
						echo ' name-stack-name';
					}
				echo '">' . $header . '</div>';
			}
			
			if($edudms_pt_name_stack !== '1') {
				echo '<div class="' . $header . ' edudms_pt_pp_header">' . $header . '</div>';
			}
		}
	edudms_pt_headers_end();
}

function edudms_pt_header_creator($headers) {
	
}

function comma_delimited_parser($string) {
	$clean_string = preg_replace( '/, /', ',', $string);
	$new_array = explode ( ',' , $clean_string);
	return $new_array;
}

function edudms_pt_add_header($field_slug, $header_text, $order, $header_set = 'main') {
	
}

function edudms_pt_headers_start() {
	echo '<div class="pp-header-block"> <!--Start Header Block-->';
}

function edudms_pt_headers_end() {
	echo '</div> <!--End Header Block-->';
}

function combine_two_pt_strings ($key_string, $value_string) {
	$key_array = comma_delimited_parser($key_string);
	$value_array = comma_delimited_parser($value_string);
	$combined_array = array_combine($key_array, $value_array);
	return $combined_array;
	
}



function edudms_pt_list_person_output($user_identifier, $fields, $headers, $css_class = 'list') {
	$fields_array = combine_two_pt_strings($headers, $fields);
	$edudms_pt_name_stack = get_option('edudms_pt_name_stack');
	foreach($fields_array as $header=>$field) {
	
	//start the name stack div
	if($field == 'full_name' and $edudms_pt_name_stack == '1') {
		echo '<div class="name-block prop-2">';
	}
		$field_value = get_user_meta($user_identifier, $field);
			//create div and css classes
			//div
			echo '<div class="show_it prop-2 ' . $css_class . ' ';
			//css classes
			echo $field;
			//finish creating div open tag
			echo '">';
			
			//prepare for mobile
			echo '<span class="mobile_header">' . $header . ': </span>';
			
			//check to see if the field is name, then check if it is goign to be a custom url or go to profile page.
			if ($field == "email") { 
				echo '<a href="mailto:'; 
				$email = get_user_meta($user_identifier, 'email');
				echo $email[0];
				echo '">';
			}
			
			//check to see if the field is name, then check if it is goign to be a custom url or go to profile page.
			if ($field == "full_name" or $field == "comma_name") { 
			echo '<a href="';
				$userdata = get_userdata($user_identifier);
				$custom_link = $userdata->data->user_url;
				$linkactive = get_user_meta($user_identifier, 'edudms_pt_linkactive');
				if ($linkactive[0] != 1 ) {
					$site_url = get_option('siteurl');
					$profile_page_id = get_option('edudms_pt_profile_page_selection');
					$profile_page_permalink = get_permalink($profile_page_id);
					echo $profile_page_permalink . '?user=' . $user_identifier;
					
				}
				if ( $linkactive[0] == 1 ) {
					echo $custom_link;
				}
				
				echo '">';
			}
			
			
			//put out the wp user avatar if "picture is specified"
			if( $field == "picture" ) {
				echo get_wp_user_avatar($user_identifier, 96, 'center');
			}
			echo $field_value[0];
			//end the anchor tag for names
			if ($field == "full_name" OR "email" or "comma_name")
				{ echo '</a>'; }
			echo '</div>';
		//end the name stack div
		if($field == 'title' and $edudms_pt_name_stack == '1') {
			echo '</div><!--end name-stack div-->';
		}
	}

}


function people_cyclone() {
	
	$people = get_users();
	//$people = new WP_User_Query( array( 'role' => 'Administrator' ) );
	
	return $people;
	
	
	
	
}







?>