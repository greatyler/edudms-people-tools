<?php

// add new buttons
add_filter( 'mce_buttons', 'edudms_pt_register_buttons' );

function edudms_pt_register_buttons( $buttons ) {
   array_push( $buttons, 'separator', 'edudms_pt' );
   return $buttons;
}
 
// Load the TinyMCE plugin : editor_plugin.js (wp2.5)
add_filter( 'mce_external_plugins', 'edudms_pt_register_tinymce_javascript' );

function edudms_pt_register_tinymce_javascript( $plugin_array ) {
   $plugin_array['edudms_pt'] = plugins_url( '/js/tinymce-plugin.js',__FILE__ );
   return $plugin_array;
}















?>