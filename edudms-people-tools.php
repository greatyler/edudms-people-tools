<?php
/*
Plugin Name: EduDMS People Tools
Plugin URI: https://bitbucket.org/greatyler/edudms-people-tools
Description: Add Extensive User and People Management Tools for WordPress
Version: 2.6.1
Author: Tyler Pruitt
Author URI: http://tpruitt.capeville.wfunet.wfu.edu/tylerpress/
Text Domain: edudemia-people-tools
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Bitbucket Plugin URI: greatyler/edudms-people-tools
Bitbucket Branch: realmaster

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.
Thank You to Jo Lowe, Tommy Murphy, and Robert Vidrine for code contributions,
design consultation, and general sanity checks throughout the development process.

The following plugins were sourced or referrenced in this project:
Code from:
*** Module - Digital Measures: Robert Vidrine. Original code used to pull data from Digital Measures at WFU was provided to me by Robert Vidrine.
https://bitbucket.org/rvidrine/




Dependencies or Referrences:





License

College People is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

College Menus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/



// Base materials


include_once(dirname(__FILE__) . '/core-people_fields.php');
include_once(dirname(__FILE__) . '/core-field_functions.php');
include_once(dirname(__FILE__) . '/shortcode-profile.php');
include(dirname(__FILE__) . '/misc_tools.php');
include(dirname(__FILE__) . '/people-cycle.php');
include(dirname(__FILE__) . '/css/css_functions.php');
include(dirname(__FILE__) . '/css/css_menu_page.php');
include_once dirname(__FILE__) . '/exclude_pages.php';
include_once dirname(__FILE__) . '/api.php';
include_once dirname(__FILE__) . '/module-digital_measures.php';
include_once dirname(__FILE__) . '/core-fields.php';


function edudms_pt_css_registration() {
wp_register_style('edudms_pt', plugins_url('edudms_pt.css',__FILE__ ));
wp_enqueue_style('edudms_pt');
wp_register_style('edudms_pt_mobile', plugins_url('css/mobile_css.php',__FILE__ ));
wp_enqueue_style('edudms_pt_mobile');
}
add_action( 'wp_enqueue_scripts','edudms_pt_css_registration');

function edudms_pt_admin_bar_render() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu('edit');
}

include(dirname(__FILE__) . '/menu_page.php');


function edudms_pt_enqueue_admin_css() {
wp_register_style( 'edudms_pt_admin_css', plugins_url('css/edudms_pt_admin_css.css',__FILE__ ));
        wp_enqueue_style( 'edudms_pt_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'edudms_pt_enqueue_admin_css' );


function edudms_pt_enqueue_edudms_ptcss_to_admin() {
wp_register_style( 'edudms_pt_css_in_admin', plugins_url('edudms_pt.css',__FILE__ ));
        wp_enqueue_style( 'edudms_pt_css_in_admin' );
}

add_action( 'admin_enqueue_scripts', 'edudms_pt_enqueue_edudms_ptcss_to_admin' );



edudms_add_required_plugin('Tabby Responsive Tabs', 'tabby-responsive-tabs', true);
edudms_add_required_plugin('WP User Avatar', 'wp-user-avatar', true);


// Add settings link on plugin page
function edudms_pt_settings_link($links) { 
  $settings_link = '<a href="users.php?page=edudms_pt_people_settings_subpage_slug">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
 
add_filter("plugin_action_links_" . plugin_basename(__FILE__), 'edudms_pt_settings_link' );







// Menu Options

function change_users_to_people() {
	global $menu;
	global $submenu;
	$menu[70][0] = 'People';
	$submenu['users.php'][5][0] = 'All People';
    $submenu['users.php'][10][0] = 'Add Person';
}

add_action( 'admin_menu', 'change_users_to_people' );






//Add Main Admin Menu Page
add_action ( 'admin_menu', 'edudms_pt_menu_page');
function edudms_pt_menu_page() {
		$edudms_pt_menu_page = add_menu_page ( 'People Tools', 'People Tools', 'manage_options', 'edudms_ptslug', 'edudms_pt_menu_page_render', plugin_dir_url( __FILE__ ) . 'images/icon_cream.png' );
		add_action( 'load-' . $edudms_pt_menu_page, 'load_edudms_ptcss_in_admin' );
		$edudms_pt_people_settings_subpage = add_users_page ( 'Edudemia People Tools', 'Settings', 'manage_options', 'edudms_pt_people_settings_subpage_slug', 'edudms_pt_menu_page_render' );
	}
	function load_edudms_ptcss_in_admin(){
			// Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
			add_action( 'admin_enqueue_styles', 'enqueue_edudms_ptcss_in_admin' );
		}
	function enqueue_edudms_ptcss_in_admin(){
			wp_enqueue_script( 'edudms_pt_css_in_admin', plugins_url('edudms_pt.css',__FILE__ ) );
		}

function edudms_pt_tools_menu_page() {
	include_once dirname(__FILE__) . '/tools/tools_menu_page.php';
	add_submenu_page( 'edudms_ptslug', 'PT Advanced Tools', 'Advanced Tools', 'manage_options', 'edudms_pt_tools_slug', 'edudms_pt_tools_menu_page_render' );
}
add_action ( 'admin_menu', 'edudms_pt_tools_menu_page');








// Plugin Activation Functions

register_activation_hook( __FILE__, 'edudms_pt_activate' );



function edudms_pt_activate() {
	if( is_plugin_active( 'edudemia-dms/edudemia-dms.php') ) {
		
	}
	else {
		die();
	}
	
	$edudms_pt_tab_labels = get_option('edudms_pt_tab_labels');
	if( $edudms_pt_tab_labels == null ) {
	update_option('edudms_pt_tab_labels', 'Bio:CV:Publications:Courses');
	}
	
	
	
	//remove_role( 'faculty' );
	if( get_role('faculty') == null ) {
	add_role( 'faculty', 'Faculty', array(
				'read'				=> true,
				'upload_files'		=> true
			)
		);
	}
}







// Plugin Deactivation



function edudms_pt_deactivation() {
 
    // Our post type will be automatically removed, so no need to unregister it
 
    // Clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
 
}

register_deactivation_hook( __FILE__, 'edudms_pt_deactivation' );


// Shortcodes


add_shortcode ('edudms_pt', 'edudms_pt_show');
add_shortcode ('pt_list', 'edudms_pt_list_start');
add_shortcode ('end_pt_list', 'edudms_pt_list_end');
add_shortcode ('people', 'edudms_pt_show');




define( 'EDUDMS_PT_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );






?>