<?php



// Create member_types field in settings
function member_type_field() {
?>
<tr>
			<th><label>Member Type</label></th>
             <td>
				<?php edudms_pt_create_member_type_select(); ?>
                <span class="description">Select what category this person is. This will affect which pages this user's information will appaer on.</span>
            </td>
        </tr> 
<?php }


function edudms_pt_member_type_input_callback_function() {
	edudms_pt_create_member_type_array();
	?>
	<input type="text" name="edudms_pt_member_type_input" id="edudms_pt_member_type_input" size="45" value="<?php echo get_option('edudms_pt_member_type_input'); ?>">
		<label class="label2" for="edudms_pt_member_type_input">Separate each Type with a Comma (Example: Speaker, Tyler Pruitt National Award Winner, Graduate Student)</label>
		<div class="label1">Enter into the box above the types of People (Members) you will allow on this site. You can select these values (in addition to faculty/staff/hidden) in each person's profile.</div>
<?php }

function edudms_pt_member_type_input_add_settings_field() {
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_member_type_input' );
	add_settings_field(
		'edudms_pt_member_type_input',
		'Member Types',
		'edudms_pt_member_type_input_callback_function',
		'edudms_pt_people_tools_options_page',
		'edudms_pt_tabbed_fields_section'
	);
}

function edudms_pt_create_member_type_array() {
	$member_types_string = 'hidden,faculty,staff,' . get_option('edudms_pt_member_type_input');
	$member_types_array = explode(',', $member_types_string);
	return $member_types_array;
}

function edudms_pt_create_member_type_select() {
	$member_types = edudms_pt_create_member_type_array();
	$current_value_array = get_user_meta( $GLOBALS["user_id"], 'edudms_pt_member_type' );
	$current_value = $current_value_array[0];
	?>
	<select name="edudms_pt_member_type" id="edudms_pt_member_type">
  <option>Choose one</option>
  <?php
    foreach($member_types as $type) { ?>
      <option value="<?= $type; ?>" <?php if($current_value == $type) { echo 'selected="selected"'; }?>><?= $type; ?></option>
	<?php }
   ?>
</select> 
<?php }


































?>