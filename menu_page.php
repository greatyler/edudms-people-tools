<?php

function edudms_pt_menu_page_render(){
		edudms_pt_change_role();
		?>
		<table>
			<tr>
				<td>
					<img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/icon32.png'; ?>" />
				</td>
				<td>
					<h1>EduDMS People Tools Options Page</h1>
				</td>
			</tr>
		</table>
		
		
		<div class=wrap>
			<form method="post" action="options.php">
				<?php settings_fields( 'edudms_pt_people_tools_options_page' ); ?>
				<?php submit_button(); ?>
				<?php do_settings_sections ( 'edudms_pt_people_tools_options_page' ); ?>

				<?php submit_button(); ?>
				<?php do_settings_sections ( 'edudms_pt_page_options_section' ); ?>
			</form>
		</div>
			



		
		<?php
		
}

include_once dirname(__FILE__) . '/wp_avatar_customize.php';
include_once dirname(__FILE__) . '/custom_shortfields.php';

//Register Options Sections

function edudms_pt_fields_api_init() {
	add_settings_section(
		'edudms_pt_tabbed_fields_section',
		'People Editing Area Options',
		'edudms_pt_tabbed_fields_section_callback_function',
		'edudms_pt_people_tools_options_page'
	);
	add_settings_section(
		'edudms_pt_page_options_section',
		'Pages Options',
		'edudms_pt_page_options_section_callback_function',
		'edudms_pt_people_tools_options_page'
	);
	add_settings_section(
		'edudms_pt_operation_section',
		'Operational Settings',
		'edudms_pt_operation_section_callback_function',
		'edudms_pt_people_tools_options_page'
	);
	

	//Profile Page Options
	
	
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_profile_page_selection' );

	
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_allow_custom_links' );
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_name_stack' );
	

	
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_profile_page_okay' );
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_tinymce_buttons_setting' );
	
	
	
	


	

	
	edudms_pt_member_type_input_add_settings_field();

	add_settings_field(
 		'edudms_pt_profile_page_selection',
 		'Profile Page',
 		'edudms_pt_profile_page_selection_callback_function',
 		'edudms_pt_people_tools_options_page',
 		'edudms_pt_page_options_section'
 	);
	add_settings_field(
 		'edudms_pt_allow_custom_links',
 		'Allow Custom Links',
 		'edudms_pt_allow_custom_links_callback_function',
 		'edudms_pt_people_tools_options_page',
 		'edudms_pt_page_options_section'
 	);
	add_settings_field(
 		'edudms_pt_name_stack',
 		'Stack Name and Tile',
 		'edudms_pt_name_stack_callback_function',
 		'edudms_pt_people_tools_options_page',
 		'edudms_pt_page_options_section'
 	);

	register_setting_for_wp_avatar();

}


add_action( 'admin_init', 'edudms_pt_fields_api_init' );

// Sections Callback Functions
	
function edudms_pt_tabbed_fields_section_callback_function() {
	?>
	<div class="instructions1">Put into each text box a list of fields you want. Use a COLON ":" to seperate each value.</div>
	<div class="instructions1">After creating your lists of custom fields for department members, they will be available for users to edit at the Profile Editing Areas.</div>
	
	
<?php }

function edudms_pt_expansion_fields_section_callback_function() {
  
}

function edudms_pt_page_options_section_callback_function() {
	
}

function edudms_pt_pp_fields_section_callback_function() {
	echo '<div class="edudms_pt-bulk-text">What you see in this preview may not reflect colors, styles, and actual alignment of your people Page. Please view the actual page to check the design. Click here to view<br />&nbsp;</div>';
	
	echo '<div class="admin_preview preview-wrapper">
	<div class="pp-header-block"> <!--Start Header Block-->
			<div class="edudms_pt_pp_header">Name</div>';
			
	if (get_option('edudms_pt_pp_name_and_title_layout_selection') !== '3') { echo '<div class="edudms_pt_pp_header">Title</div>'; }
			
	echo	'<div class="edudms_pt_pp_header">Email</div>
			<div class="edudms_pt_pp_header">Phone</div>
			<div class="edudms_pt_pp_header">Office</div>';
		
	if (get_option('edudms_pt_pp_block_6_selection') !== 1 ) { echo '<div class="edudms_pt_pp_header">Extra Column</div>'; }
	
	echo	'</div> <!--End Header Block-->
			<div class="person-block"> <!--Start Person Block 1-->';
			
	if (get_option('edudms_pt_pp_name_and_title_layout_selection') !== '2') { echo '<div class="wrap_it prop-2">'; } 
	
	echo	'<div class="show_it prop-2"><a href="http://localhost/test4/index.php/profile-page/?user=1">Tyler Pruitt</a></div>
			<div class="show_it prop-2">Instructional Tech Specialist</div>';
	
	if (get_option('edudms_pt_pp_name_and_title_layout_selection') !== '2') { echo '</div>'; }	
	
	echo	'<div class="show_it prop-2"><a href="mailto:pruitttr@wfu.edu">pruitttr@wfu.edu</a></div>
			<div class="show_it prop-2"></div>
			<div class="show_it prop-2">Tribble C2A</div>';
		
	if (get_option('edudms_pt_pp_block_6_selection') !== 1 ) { echo '<div class="show_it prop-2">Extra Column Info</div>'; }
	
	echo	'</div> <!--End Person Block 1-->
			</div> <!-- End Preview Wrapper -->';
}


function edudms_pt_operation_section_callback_function() {
	
}




// Tabbed Fields Callback Functions



function edudms_pt_profile_page_selection_callback_function() {
	
	
	$edudms_pt_profile_page_selection_current_value = get_option( 'edudms_pt_profile_page_selection' );
	$edudms_pt_profile_page_selection_args = array(
		'depth'                 => 0,
		'child_of'              => 0,
		'selected'              => $edudms_pt_profile_page_selection_current_value,
		'echo'                  => 1,
		'name'                  => 'edudms_pt_profile_page_selection',
		'id'                    => 'edudms_pt_profile_page_selection',
		'class'                 => 'You must select a page',
		'show_option_none'      => null, // string
		'show_option_no_change' => null, // string
		'option_none_value'     => null, // string
	);
	wp_dropdown_pages( $edudms_pt_profile_page_selection_args );
	?>
	<div class="label1">Select a page you've already created</div>
	
	
<?php } 



function edudms_pt_allow_custom_links_callback_function() {


	echo '<input type="checkbox" id="edudms_pt_allow_custom_links" name="edudms_pt_allow_custom_links" value="1"' . checked( 1, get_option( 'edudms_pt_allow_custom_links' ), false ) . ' />'; 
	echo '<label class="label2" for="edudms_pt_allow_custom_links">Checking this box will allow users to put in a URL that their name will link to.</label>';
}

function edudms_pt_name_stack_callback_function() {


	echo '<input type="checkbox" id="edudms_pt_name_stack" name="edudms_pt_name_stack" value="1"' . checked( 1, get_option( 'edudms_pt_name_stack' ), false ) . ' />'; 
	echo '<label class="label2" for="edudms_pt_name_stack">Checking this box will stack a Person\'s name above their Title in the default [people] (list format)</label>';
}





// People Page Callback Functions


function edudms_pt_pp_layout_pieces_callback_function() {
	
}






function edudms_pt_tinymce_buttons_setting_callback_function() {
     
    echo '<input type="checkbox" id="edudms_pt_tinymce_buttons_setting" name="edudms_pt_tinymce_buttons_setting" value="1"' . checked( 1, get_option( 'edudms_pt_tinymce_buttons_setting' ), false ) . ' />';
	echo '<label class="label2" for="edudms_pt_tinymce_buttons_setting">Checking this box will allow users to fill in their "Publications" Tab via their People Editing Area.</label>';
}






?>
