<?php

$pt_settings_icon = EDUDMS_PT_PLUGIN_PATH . 'images/icon32.png';

function edudms_pt_tools_menu_page_render(){
		edudms_pt_change_role();
		?>
		<table>
			<tr>
				<td>
					<img src="<?php echo $GLOBALS['pt_settings_icon']; ?>" />
				</td>
				<td>
					<h1>EduDMS People Tools Options Page</h1>
				</td>
			</tr>
		</table>
		
		
		<div class=wrap>
			<form method="post" action="<?php echo plugin_dir_url(__FILE__) . 'field_renamer.php'; ?>">
				<div class="menu_label">Original Field Name</div>
						<div class="option">
								<input name="original_name" id="original_name">
							</div>
						
				<div class="menu_label">New Field Name</div>
						<div class="option">
								<input name="new_name" id="new_name">
							</div>
				<div class="submit">
						<button type="submit" name="rename" value="rename">Re-name Field</button>
					</div>
			</form>
		</div>
			
			
			
		<div class=wrap>
			<form method="post" action="<?php echo plugin_dir_url(__FILE__) . 'import_dm.php'; ?>">
				<div class="menu_label">Department</div>
						<div class="option">
								<input name="import_dm_dept" id="import_dm_dept">
							</div>
				<div class="submit">
						<button type="submit" name="import_dm" value="import_dm">Import Digital Measures Data</button>
					</div>
			</form>
		</div>


		
		<?php
		
}


?>