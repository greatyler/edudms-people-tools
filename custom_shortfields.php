<?php





//get labels
function edudms_pt_shortfield_labels_input_add_settings_field() {
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_shortfield_labels' );
	add_settings_field(
		'edudms_pt_shortfield_labels',
		'Short Text Fields',
		'edudms_pt_shortfield_labels_callback_function',
		'edudms_pt_people_tools_options_page',
		'edudms_pt_tabbed_fields_section'
	);
}

function edudms_pt_shortfield_labels_callback_function() {
	?>
	<input type="text" name="edudms_pt_shortfield_labels" id="edudms_pt_shortfield_labels" size="45" value="<?php echo get_option('edudms_pt_shortfield_labels'); ?>">
		<label class="label2" for="edudms_pt_shortfield_labels">Separate each Short Text Field with a Colon ":" (Ex Region:Theme:Third:etc)</label>
		<div class="label1">Enter into the box above the Field Names for each Text Field.</div>
<?php }

//create array of member labels
function edudms_pt_create_shortfield_labels_array() {
	$shortfield_labels_string = get_option('edudms_pt_shortfield_labels');
	$shortfield_labels_array = explode(':', $shortfield_labels_string);
	return $shortfield_labels_array;
}


//Put the fields on the user profile editing areas

function edudms_pt_create_shortfield_fields_on_user_profile_editing_area() {

	$shortfield_labels = edudms_pt_create_shortfield_labels_array();
	if($shortfield_labels[0] != null ) {
		foreach($shortfield_labels as $key=>$label) {
			global $user_id
		?>
			<tr>
				<th><label><?php echo $label; ?></label></th>
				<td>
					<input type="text" id="<?php echo 'edudms_pt_shortfield_' . sanitize_key($label); ?>" name="<?php echo 'edudms_pt_shortfield_' . sanitize_key($label); ?>" size="40" value="<?php echo get_the_author_meta( 'edudms_pt_shortfield_' . sanitize_key($label), $user_id ); ?>">
					
					<br />
					<span class="description">Please enter your <?php echo $label; ?></span>
				</td>
			</tr>
	<?php }
	}	
}






function update_shortfields_user_meta() {
	$shortfield_labels = edudms_pt_create_shortfield_labels_array();
	
	foreach($shortfield_labels as $key=>$label) {
	global $user_id;
	update_user_meta( absint( $user_id ), 'edudms_pt_shortfield_' . sanitize_key($label), wp_kses_post( $_POST['edudms_pt_shortfield_' . sanitize_key($label)] ) );
	}
}


add_action( 'personal_options_update', 'update_shortfields_user_meta' );
add_action( 'edit_user_profile_update', 'update_shortfields_user_meta' );



//Show output of shortfields on frontend
function output_custom_shortfields($user_id = null) {
	
	if(empty($user_id)==true){
		global $user_id;
	}
	
	$shortfield_labels = edudms_pt_create_shortfield_labels_array();
	foreach($shortfield_labels as $key=>$label){
		$value = get_user_meta($user_id , 'edudms_pt_shortfield_' . sanitize_key($label), true);
		if(!empty($value)) {
			?>
			<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2"><?php echo esc_html( $label ); ?>:</span> <span class="edudms_pt_prp_value <?php echo sanitize_key($label); ?>"><?php echo esc_html( $value ); ?></span></div>
		<?php }
		
	}
	
	
	
}
















?>