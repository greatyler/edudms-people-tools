<?php




//shortcode function


function edudms_pt_show_table($field) {
	?>

	<div class="table-wrapper">
	
		<?php edudms_pt_output_table($field); ?>
	
	
	
	
	
	
	
	
	
	
	</div>
	
	
	
	
	
<?php	
	
}



function edudms_pt_output_table( $field, $member_type = 'faculty' ) {
	//var_dump($field);
	$field_id = get_pt_field_info( $field, 'ID' );
	//var_dump($field_id);
	$terms = wp_get_post_terms( $field_id, 'option' );
	
	//var_dump($terms);
	
		
	foreach($terms as $term) {
		?>
		<div class="row-block">
			<div class="<?php echo $term->name; ?> row-value">
				<?php echo $term->name; ?>
			
			</div>
			<div class="<?php echo $term->name; ?> row-content">
				
				<?php $db_key = 'pt_' . get_pt_field_info( $field, 'ID' );
				$valued_users = get_people_by_meta($db_key, $term->term_id, $member_type);
				$i = 0;
				$len = count($valued_users);
				foreach($valued_users as $user) {
						$profile_page_id = get_option('edudms_pt_profile_page_selection');
						$profile_page_permalink = get_permalink($profile_page_id);
						$permalink = $profile_page_permalink . '?user=' . $user->ID;
					echo '<a href="' . $permalink . '">';
					$full_name = $user->first_name . ' ' . $user->last_name;
					
					//echo '<div class="name">';
						echo $full_name . '</a>';;
						if ($i != $len - 1) {
							echo ', ';
						}
					//echo '</div>';
					$i++;
				}
				?>
				
				
			</div>
		</div>
		
		
		
		<?php
	}
}

function get_people_by_meta($meta_key, $meta_value, $member_type) {

	$args = array(						
		'meta_query' => array(
		'relation' => 'AND',
			array(
				'key' => $meta_key,
				'value' => $meta_value,
				'compare' => 'LIKE'
				),
			array(
				'key' => 'edudms_pt_member_type',
				'value' => $member_type,
				'compare' => 'LIKE'
				)
		)
	);

	//$user_with_value = new WP_User_Query( $args );
	
	$users_with_value = get_users( $args );
	
	return $users_with_value;
	
}



?>