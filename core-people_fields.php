<?php


include_once dirname(__FILE__) . '/member_types.php';


//Add User-People Profile Fields


add_action( 'show_user_profile', 'edudms_pt_show_profile_fields' );
add_action( 'edit_user_profile', 'edudms_pt_show_profile_fields' );
add_action( 'user_new_form', 'edudms_pt_show_profile_fields' );

 
function edudms_pt_show_profile_fields( $user ) { 


?>

    <h3>Faculty Profile Page Information</h3>
    <table class="form-table">
		<?php member_type_field(); ?>
		<?php 
			edudms_pt_fields_on_user_profile_editing_area('shortfield');
			edudms_pt_fields_on_user_profile_editing_area('dropdown');
			edudms_pt_fields_on_user_profile_editing_area('checkbox');
			edudms_pt_fields_on_user_profile_editing_area('tab');
			
		?>
        
				<?php if ( get_option( 'edudms_pt_allow_custom_links' ) == 1 ) { ?>
		
		<?php } ?>
		
    </table>
	
	
<?php }








 
function edudms_pt_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
	update_pt_user_meta();
	update_user_meta( absint( $user_id ), 'edudms_pt_member_type', wp_kses_post( $_POST['edudms_pt_member_type'] ) );
	edudms_pt_change_role( $GLOBALS['user_id'] );
	update_user_meta( $user_id, 'full_name', $_POST['first_name'] . ' ' . $_POST['last_name']  );
	update_user_meta( $user_id, 'comma_name', $_POST['last_name'] . ', ' . $_POST['first_name']  );
	update_user_meta( $user_id, 'email', $_POST['email']);
	if ( get_option( 'edudms_pt_allow_custom_links' ) == 1 ) {
	update_user_meta( $user_id, 'edudms_pt_linkactive', $_POST['edudms_pt_linkactive'] ); 
	update_user_meta( $user_id, 'edudms_pt_customlink', $_POST['edudms_pt_customlink']); }
}

add_action( 'personal_options_update', 'edudms_pt_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'edudms_pt_save_extra_profile_fields' );


function edudms_pt_add_contact_fields( $contact_methods ) {
	$contact_methods['title'] = 'Title';
	$contact_methods['phone'] = 'Phone';
	$contact_methods['office'] = 'Office Location';
	return $contact_methods;
};
add_filter( 'user_contactmethods', 'edudms_pt_add_contact_fields' );









//Kill Stuff on Profile Page (editing side)

function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";

}
add_action('admin_head','hide_personal_options');

//don't kill account management if admin

require_once(ABSPATH . 'wp-includes/pluggable.php');

if(current_user_can('remove_users') !== true) {


function hide_account_management_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'tr#password\').hide(); });</script>' . "\n";
}
add_action('admin_head','hide_account_management_options');



}


add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );
/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

        // remove the table row
        $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
        print $html;
    }
}

		

		
//put redirect checkbox beside website

add_action( 'personal_options', array ( 'add_website_redirect_checkbox', 'start' ) );
/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class add_website_redirect_checkbox
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

                
		$linkactive = get_user_meta($_GET['user_id'], 'edudms_pt_linkactive');
		if($linkactive[0] == 1) { $checked = 'checked'; }
		//$checked = 'checked';
		$stuff = 'class="regular-text code"> <input type="checkbox" id="edudms_pt_linkactive" name="edudms_pt_linkactive" value="1" ' . $checked . '> <label for="edudms_pt_linkactive">Check this box to redirect visitors to your website instead of your profile page.</label';
		$html = str_replace( 'class="regular-text code"', $stuff, $html );
        print $html;
    }
}	
		
		
		
		
		
?>