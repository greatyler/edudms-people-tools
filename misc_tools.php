<?php



include_once (dirname(__FILE__) . '/format_table.php');
include_once (dirname(__FILE__) . '/format_card.php');
//shortcode function

function edudms_pt_show($atts) {
	
	$GLOBALS['people_code_present'] = 1;
	
	ob_start();
	//start people-wrapper outside list or tile
	?>
	<p>
	<div class="people-wrapper">
	<?php
	
	$shortcode_atts = shortcode_atts( array(
        'member_type' 	=> 'faculty',
		'sort_by' 		=> 'last_name',
		'meta_key' 		=> 'edudms_pt_member_type',
		'meta_value' 	=> 'faculty',
		'format' 		=> 'list',
		'fields' 		=> 'full_name,title,email,phone,office',
		'headers' 		=> 'Name,Title,Email,Phone,Office',
		'field'			=> 'unknown',
		'style'			=> 'simple'
    ), $atts );
	
	$edudms_pt_cycle = edudms_pt_people_cycle($shortcode_atts['member_type'], $shortcode_atts['sort_by'] );

	
	
	if ($shortcode_atts['format'] == 'tile') {
		
		
		$shortcode_atts['fields'] = 'picture,full_name,title,phone,office';
		$shortcode_atts['headers'] = 'picture,full_name,title,phone,office';
		$css_class= 'tile';
		echo '<div class="tile-wrapper">';
			foreach ( $edudms_pt_cycle as $user ) {
				$user_identifier = $user->id;
				echo '<div class="person-block ' . $css_class . '"> <!--Start Person Block ' . $user_identifier . '-->';
					edudms_pt_list_person_output($user_identifier, $shortcode_atts['fields'], $shortcode_atts['headers'], 'tile');
				echo '</div> <!--End Person Block ' . $user_identifier . '-->';
			}
		echo '</div>';
	}

	
	
	if ($shortcode_atts['format'] == 'list') {
		
			
		edudms_pt_header_output($shortcode_atts['headers']);
		foreach ( $edudms_pt_cycle as $user ) {
		$user_identifier = $user->id;
		echo '<div class="person-block"> <!--Start Person Block ' . $user_identifier . '-->';
		edudms_pt_list_person_output($user_identifier, $shortcode_atts['fields'], $shortcode_atts['headers']);
		echo '</div> <!--End Person Block ' . $user_identifier . '-->';
		}

	
	}
	
	//end people-wrapper outside list or tile
	echo '</div>';
	echo '</p>';
	
	if ($shortcode_atts['format'] == 'table') {
		edudms_pt_show_table($shortcode_atts['field']);
		
	}
	
	if ($shortcode_atts['format'] == 'card') {
		edudms_pt_show_card($shortcode_atts['field']);
		
	}
	
	
	
	$buffer_stuff = ob_get_clean();
	return $buffer_stuff;
	
	
}




add_action('init', 'edudms_pt_profile_page_maker');

function edudms_pt_profile_page_maker() {
	
	$edudms_pt_profile_page_selection = get_option('edudms_pt_profile_page_selection');
	if(empty($edudms_pt_profile_page_selection)) {
		$new_profile_page_id = wp_insert_post( array(
		'post_type'		=>	'page',
		'post_title'	=>	'Profile Page',
		'post_status'	=>	'publish',
		'post_content'	=>	'[profile_page]'
		));
		update_option( 'edudms_pt_profile_page_selection', $new_profile_page_id );
	}
}

function settings_for_people_page_automatic_setup() {
	
}





add_shortcode( 'field_table', 'show_field_table' );

function show_field_table($field, $member_type = 'faculty', $sort_by = 'last_name') {
	
	$people = edudms_pt_people_cycle();
	$fields_checkboxes = edudms_pt_create_customdropdown_labels_array();;
	print_r($fields);
	
	?>
	
	<div class="field_table_wrapper">
	
	<?php
	
	//foreach ($fields as $key=>$field) {
		?>
			<div class="<?php echo $field; ?>">test</div>
		<?php
	//}
	
	?>
	</div><!-- end field table wrapper -->
	<?php

	
}

function output_people_for_field_table() {
	
}



add_action( 'admin_bar_menu', 'toolbar_link_to_mypage', 999 );
add_action( 'admin_bar_menu', 'view_person_admin_bar_link', 999 );
add_action( 'admin_bar_menu', 'add_person_admin_bar_link', 999 );

function toolbar_link_to_mypage( $wp_admin_bar ) {
	
	$edit_person_url = get_site_url() . '/wp-admin/user-edit.php?user_id=' . $_GET['user'];
	
	$profile_page_id = get_option('edudms_pt_profile_page_selection');
	if(is_page($profile_page_id)) {
		
			$args = array(
				'id'    => 'edit_person',
				'title' => 'Edit Person',
				'href'  => $edit_person_url
			);
			$wp_admin_bar->add_node( $args );
			$wp_admin_bar->remove_node( 'edit' );
	}
}

function view_person_admin_bar_link( $wp_admin_bar ) {
	
	$profile_page_id = get_option('edudms_pt_profile_page_selection');
	$view_person_url = get_permalink($profile_page_id) . '?user=' . $GLOBALS['user_id'];
	
	
	if( $GLOBALS['pagenow'] == 'user-edit.php' ) {
		
			$args = array(
				'id'    => 'view_person',
				'title' => 'View Person',
				'href'  => $view_person_url
			);
			$wp_admin_bar->add_node( $args );
			
	}
}


// Add Person Links

function add_person_admin_bar_link( $wp_admin_bar ) {
	
	$profile_page_id = get_option('edudms_pt_profile_page_selection');
	$add_person_url = get_site_url() . '/wp-admin/user-new.php';
	
	if( current_user_can('create_users') ) {
		if( $GLOBALS['people_code_present'] == 1 OR $GLOBALS['pagenow'] == $profile_page_id ) {
			
				$args = array(
					'id'    => 'add_new_person',
					'title' => 'Add New Person',
					'href'  => $add_person_url
				);
				$wp_admin_bar->add_node( $args );
			
				$wp_admin_bar->remove_node( 'new-user' );
		}
	}
}






//role management

function edudms_pt_change_role() {
	//$member_type = get_user_meta( $user_id, 'edudms_pt_member_type' );
	//$current_user = get_current_user_id();

		$args = array (
			'meta_key'           => 'edudms_pt_member_type',
			'meta_value'			=> 'faculty'
		);
		$user_query = get_users( $args );
		
		foreach($user_query as $person) {
				if(!user_can( $person->ID, 'manage_options')) {
					wp_update_user( array( 'ID' => $person->ID, 'role' => 'faculty' ) );
				}
		}
	}

	
function edudms_pt_colon_explode($string) {
	$array = explode( ':', $string );
	
	return $array;
}



function edudms_pt_lite_setup() {
	
	
}

function return_to_submitter_page() {
	$newURL = get_site_url() . '/wp-admin/users.php';
	header('Location: '.$newURL);
}


// Update profile buttons on profile editing area

add_action('media_buttons', 'add_update_profile_buttons');
function add_update_profile_buttons() {
	if( $GLOBALS['pagenow'] == 'profile.php' or $GLOBALS['pagenow'] == 'user_edit.php' ) {
    echo '<input type="submit" name="submit" id="submit" class="button button-primary" value="Save All Changes">';
	}
}








?>