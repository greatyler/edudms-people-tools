<?php

function add_option_edudms_pt_operation_defaults() {
	add_option('edudms_pt_wp_avatar_control', 1);
}

function register_setting_for_wp_avatar() {
	register_setting( 'edudms_pt_people_tools_options_page', 'edudms_pt_wp_avatar_control' );
	add_settings_field(
		'edudms_pt_wp_avatar_control',
		'WP Avatar Auto Control Enable',
		'edudms_pt_wp_avatar_control_callback_function',
		'edudms_pt_people_tools_options_page',
		'edudms_pt_operation_section'
	);
}


function edudms_pt_wp_avatar_control_callback_function() {
	echo '<input type="checkbox" id="edudms_pt_wp_avatar_control" name="edudms_pt_wp_avatar_control" value="1"' . checked( 1, get_option( 'edudms_pt_wp_avatar_control' ), false ) . ' />';
	echo '<label class="label2" for="edudms_pt_bio_field_setting">Checking this box will allow EduDMS PT to manage the options for WP User Avatar Plugin. Leaving this enabled will ensure the plugin works as intended.</label>';
	echo '<div class="label1">Recommended: Enabled</div>';
}


function setup_wp_avatar() {
	if(get_option('edudms_pt_wp_avatar_control') == 1) {
	update_option( 'wp_user_avatar_disable_gravatar', 1);
	}
}


add_action( 'init','add_option_edudms_pt_operation_defaults');















?>